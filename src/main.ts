import { AIFiringMode } from "ftd-typescript-definitions";
import { add } from "./example";

let helloWorld = 'Hello World!';
function Update(I: I) {
  I.Log(helloWorld);
  I.Log(add(1,1).toString());
  I.Log(AIFiringMode.Off);
}